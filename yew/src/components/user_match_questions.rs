use yew::prelude::*;

pub struct Questions {
    link: ComponentLink<Self>,
}

pub enum Msg {
    Answer,
}

impl Component for Questions {
    type Message = Msg;
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Questions {
            link
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="questions">
                <ul>
                    <li>{ "Who?" }</li>
                    <li>{ "What?" }</li>               
                </ul>
            </div>
        }
    }
}
