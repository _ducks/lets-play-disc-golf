CREATE TABLE course (
  id SERIAL PRIMARY KEY,
  name VARCHAR(80) NOT NULL,
  nick VARCHAR(80),
  city_id INT REFERENCES city(id) NOT NULL,
  country_id INT REFERENCES country(id) NOT NULL,
  district_id INT REFERENCES district(id) NOT NULL,
  postal_code VARCHAR(50) NOT NULL,
  last_updated TIMESTAMP NOT NULL
);

INSERT 
  INTO COURSE (
    name, nick, city_id, country_id, district_id, postal_code, last_updated
  )
  VALUES 
    ('Milo McIver State Park', '', 1, 1, 1, 97023, NOW()),
    ('Pier Park', '', 2, 1, 1, 97203, NOW()),
    ('Maple Hill DGC', '', 3, 1, 2, 01524, NOW());
