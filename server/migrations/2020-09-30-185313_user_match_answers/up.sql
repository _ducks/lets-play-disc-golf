CREATE TABLE user_match_answers (
  id SERIAL PRIMARY KEY,
  answer VARCHAR NOT NULL,
  created TIMESTAMP NOT NULL,
  last_updated TIMESTAMP NOT NULL
);

INSERT 
  INTO user_match_answers (answer, created, last_updated)
  VALUES 
    ('very unlikely', NOW(), NOW()),
    ('somewhat unlikely', NOW(), NOW()),
    ('neutral', NOW(), NOW()),
    ('somewhat likely', NOW(), NOW()),
    ('very likely', NOW(), NOW());
