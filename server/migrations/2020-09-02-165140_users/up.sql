CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name VARCHAR(80) NOT NULL,
  nick VARCHAR(80),
  city VARCHAR(80),
  postal_code INT,
  age INT,
  created TIMESTAMP NOT NULL,
  last_updated TIMESTAMP NOT NULL
);

INSERT 
  INTO USERS (name, nick, city, postal_code, age, created, last_updated)
  VALUES('robert', 'bob', 'Portland', 12345, 55, NOW(), NOW());
