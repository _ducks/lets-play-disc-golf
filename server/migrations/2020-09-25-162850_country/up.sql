CREATE TABLE country (
  id SERIAL PRIMARY KEY,
  name VARCHAR(80) NOT NULL,
  code VARCHAR(10) NOT NULL,
  last_updated TIMESTAMP NOT NULL
);

INSERT 
  INTO COUNTRY (name, code, last_updated)
  VALUES ('United States', 'US', NOW());
