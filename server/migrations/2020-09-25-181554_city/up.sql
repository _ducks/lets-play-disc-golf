CREATE TABLE city (
  id SERIAL PRIMARY KEY,
  name VARCHAR(80) NOT NULL,
  last_updated TIMESTAMP NOT NULL
);

INSERT
  INTO city (name, last_updated)
  VALUES 
    ('estacada', NOW()),
    ('portland', NOW()),
    ('leicester', NOW());
