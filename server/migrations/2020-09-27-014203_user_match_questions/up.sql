CREATE TABLE user_match_questions (
  id SERIAL PRIMARY KEY,
  question VARCHAR NOT NULL,
  created TIMESTAMP NOT NULL,
  last_updated TIMESTAMP NOT NULL
);

INSERT 
  INTO user_match_questions (question, created, last_updated)
  VALUES 
    ('how likely are you to play with someone who smokes tobacco?', NOW(), NOW()),
    ('how likely are you to play with someone who has a dog?', NOW(), NOW()),
    (
      'how likely are you to play with someone who smokes cannabis/marijuana?', 
      NOW(),
      NOW()
    ),
    (
      'how likely are you to play with someone who plays audible music?', 
      NOW(),
      NOW()
    ),
    (
      'how likely are you to play with someone who drinks alcohol?',
      NOW(),
      NOW()
    );
