CREATE TABLE district (
  id SERIAL PRIMARY KEY,
  name VARCHAR(80) NOT NULL,
  code VARCHAR(5) NOT NULL,
  last_updated TIMESTAMP NOT NULL
);

INSERT
  INTO district (name, code, last_updated)
  VALUES 
    ('oregon', 'or', NOW()),
    ('massachusetts', 'ma', NOW());
