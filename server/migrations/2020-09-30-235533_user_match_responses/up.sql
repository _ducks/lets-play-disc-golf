CREATE TABLE user_match_responses (
  id SERIAL,
  user_id INT REFERENCES users(id) NOT NULL,
  question_id INT REFERENCES user_match_questions(id) NOT NULL,
  answer_id INT REFERENCES user_match_answers(id) NOT NULL,
  created TIMESTAMP NOT NULL,
  last_updated TIMESTAMP NOT NULL,
  PRIMARY KEY (user_id, question_id)
);

INSERT
  INTO user_match_responses (
    user_id, question_id, answer_id, created, last_updated
  )
  VALUES (
    1, 1, 1, NOW(), NOW()
  );
