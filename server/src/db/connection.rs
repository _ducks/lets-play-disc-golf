use rocket_contrib::database;

#[database("postgres")]
pub struct PostgresConn(pub diesel::PgConnection);
