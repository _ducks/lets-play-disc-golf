use crate::schema::user_match_responses;
use chrono::prelude::*;

#[derive(Debug, Queryable, Clone)]
pub struct UserMatchResponse {
    pub id: i32,
    pub user_id: i32,
    pub question_id: i32,
    pub answer_id: i32,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}

#[derive(Debug, Insertable, Clone)]
#[table_name="user_match_responses"]
pub struct NewUserMatchResponse {
    pub user_id: i32,
    pub question_id: i32,
    pub answer_id: i32,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}
