use crate::schema::course;
use chrono::prelude::*;

#[derive(Clone, Debug, Queryable, Identifiable)]
#[table_name="course"]
pub struct Course {
    pub id: i32,
    pub name: String,
    pub nick: Option<String>,
    pub city_id: i32,
    pub country_id: i32,
    pub district_id: i32,
    pub postal_code: String,
    pub last_updated: NaiveDateTime
}
