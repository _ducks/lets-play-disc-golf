use crate::schema::country;
use chrono::prelude::*;

#[derive(Clone, Debug, Queryable, Identifiable)]
#[table_name="country"]
pub struct Country {
    pub id: i32,
    pub name: String,
    pub code: String,
    pub last_updated: NaiveDateTime
}
