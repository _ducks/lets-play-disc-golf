use crate::schema::district;
use chrono::prelude::*;

#[derive(Clone, Debug, Queryable, Identifiable)]
#[table_name="district"]
pub struct District {
    pub id: i32,
    pub name: String,
    pub code: String,
    pub last_updated: NaiveDateTime
}
