use crate::schema::user_match_answers;
use chrono::prelude::*;

#[derive(Debug, Queryable, Clone)]
pub struct UserMatchAnswer {
    pub id: i32,
    pub answer: String,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}
