use crate::schema::users;
use chrono::prelude::*;

#[derive(Debug, Queryable, Identifiable, Clone)]
#[table_name="users"]
pub struct User {
    pub id: i32,
    pub name: String,
    pub nick: Option<String>,
    pub city: Option<String>,
    pub postal_code: Option<i32>,
    pub age: Option<i32>,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}

#[derive(Debug, Insertable)]
#[table_name="users"]
pub struct NewUser {
    pub name: String,
    pub nick: Option<String>,
    pub city: Option<String>,
    pub postal_code: Option<i32>,
    pub age: Option<i32>,
    pub last_updated: NaiveDateTime
}
