use crate::schema::user_match_questions;
use chrono::prelude::*;

#[derive(Debug, Queryable, Clone)]
pub struct UserMatchQuestion {
    pub id: i32,
    pub question: String,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}
