use crate::schema::city;
use chrono::prelude::*;

#[derive(Clone, Debug, Queryable, Identifiable)]
#[table_name="city"]
pub struct City {
    pub id: i32,
    pub name: String,
    pub last_updated: NaiveDateTime
}
