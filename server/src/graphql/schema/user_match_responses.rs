use chrono::prelude::*;
use diesel::prelude::*;
use juniper::{FieldResult};

use crate::graphql::Context;
use crate::schema::users;
use crate::schema::user_match_responses;
use crate::db::models;

#[derive(Debug, GraphQLObject)]
pub struct UserMatchResponse {
    pub id: i32,
    pub user_id: i32,
    pub question_id: i32,
    pub answer_id: i32,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}

#[derive(Debug, GraphQLInputObject)]
pub struct NewUserMatchResponse {
    pub user_id: i32,
    pub question_id: i32,
    pub answer_id: i32,
}

impl From<models::UserMatchResponse> for UserMatchResponse {
    fn from(response: models::UserMatchResponse) -> Self {
        UserMatchResponse {
            id: response.id,
            user_id: response.user_id,
            question_id: response.question_id,
            answer_id: response.answer_id,
            created: response.created,
            last_updated: response.last_updated,
        }
    }
}

impl Context {
    pub fn get_responses_by_user_id(&self, user_id: i32) -> 
        FieldResult<Vec<UserMatchResponse>> {

        let results = user_match_responses::table
            .inner_join(users::table)
            .filter(users::id.eq(user_id))
            .load::<(
                models::UserMatchResponse, models::User
             )>(&*self.connection)?;

        let r = results
            .iter()
            .map(|(r, u)|
                 UserMatchResponse::from(r.clone())
                ).collect();

        Ok(r)
    }

    pub fn create_user_match_response(&self, input: NewUserMatchResponse) -> 
        FieldResult<UserMatchResponse> {

        let response = models::NewUserMatchResponse {
            user_id: input.user_id,
            question_id: input.question_id,
            answer_id: input.answer_id,
            created: Utc::now().naive_utc(),
            last_updated: Utc::now().naive_utc(),
        };

        let r = diesel::insert_into(user_match_responses::table)
            .values(&response)
            .get_result::<models::UserMatchResponse>(&*self.connection)
            .expect("Adding user error");

        Ok(r.into())
    }
}
