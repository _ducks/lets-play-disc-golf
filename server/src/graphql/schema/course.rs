use chrono::NaiveDateTime;
use diesel::prelude::*;
use juniper::FieldResult;

use crate::db::models;

use crate::graphql::Context;
use crate::graphql::schema::{City, Country, District};

use crate::schema::{
    city,
    country,
    course,
    district
};

#[derive(Debug, GraphQLObject)]
pub struct Course {
    pub id: i32,
    pub name: String,
    pub nick: Option<String>,
    pub city: City, 
    pub country: Country,
    pub district: District,
    pub postal_code: String,
    pub last_updated: NaiveDateTime
}

impl From<(
    models::Course, models::District, models::City, models::Country
)> for Course {
    fn from(input: (
        models::Course, 
        models::District, 
        models::City,
        models::Country
    )) -> Self {
        let (c, d, ci, co) = input;

        Course {
            id: c.id,
            name: c.name,
            nick: c.nick,
            city: City { 
                name: ci.name 
            },
            country: Country {
                name: co.name,
                code: co.code,
            },
            district: District {
                name: d.name,
                code: d.code,
            },
            postal_code: c.postal_code,
            last_updated: c.last_updated
        }
    }
}

#[derive(Debug, GraphQLObject)]
pub struct Courses {
    pub items: Vec<Courses>,
}

impl Context {
    pub fn get_course_by_id(&self, id: i32) -> FieldResult<Course> {
        let (course, district, city, country) = course::table
            .find(id)
            .inner_join(district::table)
            .inner_join(city::table)
            .inner_join(country::table)
            .first::<(models::Course,
                models::District,
                models::City,
                models::Country
            )>(&*self.connection)?;

        let c = Course::from((course, district, city, country));

        Ok(c)
    }

    pub fn get_all_courses(&self) -> FieldResult<Vec<Course>> {
        let results = course::table
            .inner_join(district::table)
            .inner_join(city::table)
            .inner_join(country::table)
            .load::<(models::Course,
                models::District,
                models::City,
                models::Country
            )>(&*self.connection);

        let r = results
            .unwrap()
            .iter()
            .map(|(c, d, ci, co)|
                 Course::from((c.clone(), d.clone(), ci.clone(), co.clone()))
             ).collect();


        Ok(r)

    }

    pub fn get_courses_by_district(&self, dist: String) -> 
        FieldResult<Vec<Course>> {

        let results = course::table
            .inner_join(district::table)
            .inner_join(city::table)
            .inner_join(country::table)
            .filter(district::name.eq(dist))
            .load::<(models::Course,
                models::District,
                models::City,
                models::Country
            )>(&*self.connection);

        let r = results
            .unwrap()
            .iter()
            .map(|(c, d, ci, co)|
                 Course::from((c.clone(), d.clone(), ci.clone(), co.clone()))
             ).collect();


        Ok(r)

    }
}
