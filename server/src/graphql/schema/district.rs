#[derive(Debug, GraphQLObject)]
pub struct District {
    pub name: String,
    pub code: String,
}
