use juniper::{RootNode};

mod city;
mod course;
mod country;
mod district;
mod user;
mod user_match_questions;
mod user_match_answer;
mod user_match_responses;

mod mutation;
mod query;

pub use city::City;
pub use course::*;
pub use country::Country;
pub use district::District;
pub use user::*;
pub use user_match_questions::{UserMatchQuestion};
pub use user_match_answer::UserMatchAnswer;
pub use user_match_responses::{NewUserMatchResponse, UserMatchResponse};
pub use mutation::*;
pub use query::*;

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;
