#[derive(Debug, GraphQLObject)]
pub struct Country {
    pub name: String,
    pub code: String
}
