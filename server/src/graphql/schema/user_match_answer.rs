use chrono::prelude::*;
use diesel::prelude::*;
use juniper::{FieldResult};

use crate::graphql::Context;
use crate::schema::user_match_answers;
use crate::db::models;

#[derive(Debug, GraphQLObject)]
pub struct UserMatchAnswer {
    pub id: i32,
    pub answer: String,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}

impl From<models::UserMatchAnswer> for UserMatchAnswer {
    fn from(answer: models::UserMatchAnswer) -> Self {
        UserMatchAnswer {
            id: answer.id,
            answer: answer.answer,
            created: answer.created,
            last_updated: answer.last_updated,
        }
    }
}

impl Context {
    pub fn get_all_answers(&self) -> FieldResult<Vec<UserMatchAnswer>> {
        let answers = user_match_answers::table.load::<
            models::UserMatchAnswer
        >(&*self.connection)?;

        let parsed_answers = answers 
            .iter()
            .map(|a| UserMatchAnswer::from(a.clone())).collect();

        Ok(parsed_answers)
    }
}
