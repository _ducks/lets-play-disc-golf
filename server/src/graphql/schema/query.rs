use juniper::FieldResult;

use crate::graphql::schema::User;
use crate::graphql::schema::Course;
use crate::graphql::schema::UserMatchQuestion;
use crate::graphql::schema::UserMatchAnswer;
use crate::graphql::schema::{NewUserMatchResponse, UserMatchResponse};
use crate::graphql::Context;

#[derive(Debug)]
pub struct QueryRoot;

graphql_object!(QueryRoot: Context |&self| {
    field user(&executor, id: i32) -> FieldResult<User> {
        executor.context().get_user_by_id(id)
    }

    field users(&executor) -> FieldResult<Vec<User>> {
        executor.context().get_all_users()
    }

    field course(&executor, id: i32) -> FieldResult<Course> {
        executor.context().get_course_by_id(id)
    }

    field courses(&executor) -> FieldResult<Vec<Course>> {
        executor.context().get_all_courses()
    }

    field coursesByDistrict(&executor, district: String) -> 
        FieldResult<Vec<Course>> {
            executor.context().get_courses_by_district(district)
        }

    field userMatchQuestions(&executor) -> FieldResult<Vec<UserMatchQuestion>> {
        executor.context().get_all_questions() 
    }

    field userMatchAnswers(&executor) -> FieldResult<Vec<UserMatchAnswer>> {
        executor.context().get_all_answers() 
    }

    field userMatchResponsesByUser(&executor, user_id: i32) -> 
        FieldResult<Vec<UserMatchResponse>> {

        executor.context().get_responses_by_user_id(user_id)
    }
});
