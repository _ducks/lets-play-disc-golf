use chrono::prelude::*;
use diesel::prelude::*;
use juniper::{FieldResult};

use crate::graphql::Context;
use crate::schema::users;
use crate::db::models;

#[derive(Debug, GraphQLObject)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub nick: Option<String>,
    pub city: Option<String>,
    pub postal_code: Option<i32>,
    pub age: Option<i32>,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}

impl From<models::User> for User {
    fn from(user: models::User) -> Self {
        User {
            id: user.id,
            name: user.name,
            nick: user.nick,
            city: user.city,
            postal_code: user.postal_code,
            age: user.age,
            created: user.created,
            last_updated: user.last_updated,
        }
    }
}

#[derive(Debug, GraphQLObject)]
pub struct Users {
    pub items: Vec<Users>,
}

#[derive(GraphQLInputObject)]
pub struct NewUser {
    pub name: String,
    pub nick: Option<String>,
    pub city: Option<String>,
    pub postal_code: Option<i32>,
    pub age: Option<i32>,
}

impl Context {
    pub fn get_user_by_id(&self, id: i32) -> FieldResult<User> {
        let user = users::table
            .find(id)
            .first::<models::User>(&*self.connection)?;

        Ok(user.into())
    }

    pub fn get_all_users(&self) -> FieldResult<Vec<User>> {
        let users = users::table.load::<models::User>(&*self.connection)?;

        let parsed_users = users
            .iter()
            .map(|user| User::from(user.clone())).collect();

        Ok(parsed_users)
    }

    pub fn create_user(&self, new_user: NewUser) -> FieldResult<User> {
        let new_user = models::NewUser {
            name: new_user.name,
            nick: new_user.nick,
            city: new_user.city,
            postal_code: new_user.postal_code,
            age: new_user.age,
            last_updated: Utc::now().naive_utc(),
        };

        let user = diesel::insert_into(users::table)
            .values(&new_user)
            .get_result::<models::User>(&*self.connection)
            .expect("Adding user error");

        Ok(user.into())
    }
}
