use juniper::FieldResult;

use crate::graphql::Context;
use crate::graphql::schema::{NewUser, User};
use crate::graphql::schema::{NewUserMatchResponse, UserMatchResponse};

pub struct MutationRoot;

graphql_object!(MutationRoot: Context |&self| {
  field addUser(&executor, new_user: NewUser) -> FieldResult<User> {
      executor.context().create_user(new_user)
  }

  field addUserMatchResponse(&executor, input: NewUserMatchResponse) -> 
        FieldResult<UserMatchResponse> {

        executor.context().create_user_match_response(input)
    }
});
