use chrono::prelude::*;
use diesel::prelude::*;
use juniper::{FieldResult};

use crate::graphql::Context;
use crate::schema::user_match_questions;
use crate::db::models;

#[derive(Debug, GraphQLObject)]
pub struct UserMatchQuestion {
    pub id: i32,
    pub question: String,
    pub created: NaiveDateTime,
    pub last_updated: NaiveDateTime
}

#[derive(Debug, GraphQLObject)]
pub struct UserMatchQuestions {
    pub questions: Vec<UserMatchQuestion>
}

impl From<models::UserMatchQuestion> for UserMatchQuestion {
    fn from(question: models::UserMatchQuestion) -> Self {
        UserMatchQuestion {
            id: question.id,
            question: question.question,
            created: question.created,
            last_updated: question.last_updated,
        }
    }
}

impl Context {
    pub fn get_all_questions(&self) -> FieldResult<Vec<UserMatchQuestion>> {
        let questions = user_match_questions::table.load::<
            models::UserMatchQuestion
        >(&*self.connection)?;

        let parsed_questions = questions 
            .iter()
            .map(|q| UserMatchQuestion::from(q.clone())).collect();

        Ok(parsed_questions)
    }
}
