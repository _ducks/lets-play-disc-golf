use crate::db::connection::PostgresConn;
use juniper::{Context as JuniperContext};

pub struct Context {
    pub connection: PostgresConn
}

impl JuniperContext for Context {}
