use rocket::response::content::Html;
use rocket::State;
use juniper_rocket::{graphiql_source, GraphQLRequest, GraphQLResponse};
use crate::graphql::{Context, Schema};
use crate::db::connection::PostgresConn;

#[get("/graphiql")]
pub fn graphiql() -> Html<String> {
    graphiql_source("/api/graphql")
}

#[get("/graphql?<request>")]
pub fn get_graphql_handler(
    request: GraphQLRequest,
    schema: State<Schema>,
    conn: PostgresConn,
) -> GraphQLResponse {
    request.execute(&schema, &Context { connection: conn })
}

#[post("/graphql", data = "<request>")]
pub fn post_graphql_handler(
    request: GraphQLRequest,
    schema: State<Schema>,
    conn: PostgresConn,
) -> GraphQLResponse {
    request.execute(&schema, &Context { connection: conn })
}
