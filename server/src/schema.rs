table! {
    city (id) {
        id -> Int4,
        name -> Varchar,
        last_updated -> Timestamp,
    }
}

table! {
    country (id) {
        id -> Int4,
        name -> Varchar,
        code -> Varchar,
        last_updated -> Timestamp,
    }
}

table! {
    course (id) {
        id -> Int4,
        name -> Varchar,
        nick -> Nullable<Varchar>,
        city_id -> Int4,
        country_id -> Int4,
        district_id -> Int4,
        postal_code -> Varchar,
        last_updated -> Timestamp,
    }
}

table! {
    district (id) {
        id -> Int4,
        name -> Varchar,
        code -> Varchar,
        last_updated -> Timestamp,
    }
}

table! {
    user_match_answers (id) {
        id -> Int4,
        answer -> Varchar,
        created -> Timestamp,
        last_updated -> Timestamp,
    }
}

table! {
    user_match_questions (id) {
        id -> Int4,
        question -> Varchar,
        created -> Timestamp,
        last_updated -> Timestamp,
    }
}

table! {
    user_match_responses (user_id, question_id) {
        id -> Int4,
        user_id -> Int4,
        question_id -> Int4,
        answer_id -> Int4,
        created -> Timestamp,
        last_updated -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        name -> Varchar,
        nick -> Nullable<Varchar>,
        city -> Nullable<Varchar>,
        postal_code -> Nullable<Int4>,
        age -> Nullable<Int4>,
        created -> Timestamp,
        last_updated -> Timestamp,
    }
}

joinable!(course -> city (city_id));
joinable!(course -> country (country_id));
joinable!(course -> district (district_id));
joinable!(user_match_responses -> user_match_answers (answer_id));
joinable!(user_match_responses -> user_match_questions (question_id));
joinable!(user_match_responses -> users (user_id));

allow_tables_to_appear_in_same_query!(
    city,
    country,
    course,
    district,
    user_match_answers,
    user_match_questions,
    user_match_responses,
    users,
);
