#![feature(decl_macro, proc_macro_hygiene)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate juniper;
#[macro_use] extern crate diesel;
extern crate juniper_rocket;

use rocket::http::Method;
use rocket_cors::{AllowedHeaders, AllowedOrigins};
use rocket_contrib::serve::StaticFiles;
use std::error::Error;

mod db;
mod graphql;
mod routes;
mod schema;

fn main() -> Result<(), Box<dyn Error>> {
    let allowed_origins = AllowedOrigins::some_exact(&[
        "http://localhost:5000",
        "http://localhost:8000"
    ]);

    let cors = rocket_cors::CorsOptions {
        allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post]
            .into_iter()
            .map(From::from)
            .collect(),
        allowed_headers: AllowedHeaders::some(&[
            "Accept",
            "Access-Control-Allow-Origin",
        ]),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors()?;

    rocket::ignite()
        .attach(db::connection::PostgresConn::fairing())
        .manage(graphql::schema::Schema::new(
            graphql::schema::QueryRoot,
            graphql::schema::MutationRoot,
        ))
        .mount("/api", routes![
            routes::graphql::graphiql, 
            routes::graphql::get_graphql_handler,
            routes::graphql::post_graphql_handler,
        ])
        .attach(cors)
        .launch();

    Ok(())
}
